import React, { Component } from "react";
import Search from "../components/Search";

class Body extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="row" style={{ marginTop: 20 }}>
				<Search />
			</div>
		);
	}
}

export default Body;
