import React from "react";
import { Link } from "react-router-dom";
function Header() {
	return (
		<div style={{ borderBottom: "1px solid #eee" }}>
			<h1>Hệ thống tìm kiếm địa danh</h1>
			<h3>
				{/* <Link to="/">Trang chủ</Link> */}
			</h3>
			<h3>
				<Link to="/">Tìm kiếm bằng từ khóa</Link>
			</h3>
			<h3>
				<Link to="/search_image">Tìm kiếm bằng hình ảnh</Link>
			</h3>
		</div>
	);
}

export default Header;
