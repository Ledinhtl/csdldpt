import React, { Component } from "react";
import axios from "axios";
import "./Search.css";
import Spinner from "react-bootstrap/Spinner";
import serverIpAdress from "../../config/config";

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			previous_search_key: "",
			current_search_key: "",
			articles: [],
			isLoading: false,
			total_pages: 0,
			current_page: 1,
			articles_amount_for_each_page: 10,
		};
		this.loadPage = this.loadPage.bind(this);
	}

	componentDidMount() {
		this.loadPage("");
	}

	loadPage = async (query = "") => {
		console.log("data before request");
		console.log({
			// category: this.props.category,
			q: query,
		});

		this.setState({ isLoading: true });
		console.log(serverIpAdress);
		const res = (
			await axios.post(`${serverIpAdress}/search_text`, {
				// category: this.props.category,
				q: query,
			})
		).data;
		console.log("res = ");
		console.log(res);

		this.setState(
			{
				previous_search_key: query,
				articles: res["docs"],
				isLoading: false,
				current_page: 1,
				total_pages: parseInt((res["docs"].length - 1) / 10 + 1),
			},
			() => console.log(this.state)
		);
	};

	render() {
		const {
			current_search_key,
			previous_search_key,
			isLoading,
			current_page,
			total_pages,
			articles_amount_for_each_page,
		} = this.state;

		const start_index = articles_amount_for_each_page * (current_page - 1);
		const end_index = articles_amount_for_each_page * current_page;
		const articlesForEachPage = this.state.articles.slice(
			start_index,
			end_index
		);

		const temp_array_mark_current_page = [];
		for (let i = 1; i <= total_pages; i++) {
			temp_array_mark_current_page.push(i);
		}

		return (
			<div className="col-md-12">
				<form
					autocomplete="off"
					style={{ marginBottom: 10 }}
					onSubmit={"return false"}
				>
					<div className="autocomplete" style={{ width: 800 }}>
						<input
							type="text"
							placeholder="Tìm kiếm"
							value={current_search_key}
							onChange={
								(e) => {
									this.setState({
										current_search_key: e.target.value,
									});
									// console.log(e.target.key);
								}
								// if (e.key === "Enter"){
								// 	this.
								// }
							}
							onKeyDown={(event) => {
								console.log(event.key);
								if (event.key === "Enter") {
									console.log("this is enter press action");
									event.preventDefault();
									this.loadPage(current_search_key);
								}
							}}
						/>
					</div>
					<p
						className="btn btn-primary"
						onClick={() => this.loadPage(current_search_key)}
						style={{ marginTop: 10, marginBottom: "10px" }}
					>
						Tìm kiếm
					</p>
				</form>
				<div
					style={{
						width: "580px",
						textAlign: "left",
						margin: "auto",
					}}
				>
					{isLoading ? (
						<Spinner animation="border" variant="primary" />
					) : previous_search_key.length !== 0 &&
					  articlesForEachPage.length === 0 ? (
						<div style={{ fontSize: "20px", marginTop: "30px" }}>
							Không tìm thấy kết quả cho
							<span
								style={{ fontWeight: "bold", fontSize: "25px" }}
							>
								{" " +
									(previous_search_key.length < 75
										? previous_search_key
										: previous_search_key.slice(0, 75) +
										  "...")}
							</span>
						</div>
					) : (
						articlesForEachPage.map((item) => (
							<div>
								<div className="row">
									<div className="col-md-5">
										<img
											src={
												`${serverIpAdress}/get_image?image_url=` +
												item["images"][0]
											}
											style={{
												width: "100%",
												borderBottom: "2px",
												marginTop: "15px",
											}}
										/>
									</div>
									<div
										className="col-md-7"
										style={{
											fontSize: "20px",
											fontWeight: "400",
											color: "#087CCE",
											marginTop: "15px",
										}}
									>
										<a
											href={"article/" + item["id"]}
											target="_blank"
										>
											{item["title"]}
										</a>
										<div style={{ color: "black" }}>
											{item["type"]}
										</div>
										<div
											style={{
												fontSize: "14px",
												color: "black",
											}}
										>
											<div>
												{item["content"]
													? item["content"][0].slice(
															0,
															300
													  ) + "....."
													: ""}
											</div>
										</div>
									</div>
								</div>
							</div>
						))
					)}

					<div style={{ marginTop: "10px", marginBottom: "30px" }}>
						{!isLoading && total_pages != 0 && (
							<ul className="pagination">
								<li
									className={`page-item first-item ${
										current_page === 1 ? "disabled" : ""
									}`}
									onClick={() =>
										this.setState({ current_page: 1 })
									}
								>
									<a>First</a>
								</li>
								<li
									className={`page-item previous-item ${
										current_page === 1 ? "disabled" : ""
									}`}
									onClick={() =>
										this.setState({
											current_page: current_page - 1,
										})
									}
								>
									<a>Previous</a>
								</li>
								{temp_array_mark_current_page.map((page) => (
									<li
										key={page}
										className={`page-item number-item ${
											current_page === page
												? "active"
												: ""
										}`}
										onClick={() =>
											this.setState({
												current_page: page,
											})
										}
									>
										<a>{page}</a>
									</li>
								))}
								<li
									className={`page-item next-item ${
										current_page === total_pages
											? "disabled"
											: ""
									}`}
									onClick={() =>
										this.setState({
											current_page: current_page + 1,
										})
									}
								>
									<a>Next</a>
								</li>
								<li
									className={`page-item last-item ${
										current_page === total_pages
											? "disabled"
											: ""
									}`}
									onClick={() =>
										this.setState({
											current_page: total_pages,
										})
									}
								>
									<a>Last</a>
								</li>
							</ul>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Search;
