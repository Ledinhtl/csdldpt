import React from "react";
function Header() {
	return (
		<div style={{ borderBottom: "1px solid #eee" }}>
			<h1>Hệ thống tìm kiếm địa danh</h1>
			<h3>Tìm kiếm bằng từ khóa</h3>
		</div>
	);
}

export default Header;
