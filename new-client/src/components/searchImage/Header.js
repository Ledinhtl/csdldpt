import React from "react";
function Header() {
	return (
		<div style={{ borderBottom: "1px solid #eee" }}>
			<h1>Hệ thống tìm kiếm địa danh</h1>
			<h3>Tìm kiếm bằng hình ảnh</h3>
		</div>
	);
}

export default Header;
