import React, { Component } from "react";
import axios from "axios";
import "./Search.css";
import Spinner from "react-bootstrap/Spinner";
import serverIpAdress from "../../config/config";

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedFile: null,
			previous_search_key: "",
			articles: [],
			isLoading: false,
			total_pages: 0,
			current_page: 1,
			articles_amount_for_each_page: 10,
			relevant_images: [],
			predicted_label: "",
			previewImageURL: "",
		};
	}

	fileSelectedHandler = (event) => {
		console.log(event.target.files[0]);
		this.setState({
			selectedFile: event.target.files[0],
			previewImageURL: URL.createObjectURL(event.target.files[0]),
		});
	};

	searchByImage = async (event) => {
		const fd = new FormData();
		fd.append(
			"image",
			this.state.selectedFile,
			this.state.selectedFile.name
		);

		this.setState({ isLoading: true });
		console.log(serverIpAdress);
		const res = (await axios.post(`${serverIpAdress}/search_image`, fd))
			.data;
		console.log("res = ");
		console.log(res);

		this.setState(
			{
				predicted_label: res["predicted_label"],
				previous_search_key: res["predicted_label"],
				articles: res["relevant_docs"],
				relevant_images: res["relevant_images"],
				isLoading: false,
				current_page: 1,
				total_pages: parseInt(
					(res["relevant_docs"].length - 1) / 10 + 1
				),
			},
			() => console.log(this.state)
		);
	};

	render() {
		const {
			previous_search_key,
			isLoading,
			current_page,
			total_pages,
			articles_amount_for_each_page,
			relevant_images,
			predicted_label,
			previewImageURL,
		} = this.state;

		const start_index = articles_amount_for_each_page * (current_page - 1);
		const end_index = articles_amount_for_each_page * current_page;
		const articlesForEachPage = this.state.articles.slice(
			start_index,
			end_index
		);

		const temp_array_mark_current_page = [];
		for (let i = 1; i <= total_pages; i++) {
			temp_array_mark_current_page.push(i);
		}

		return (
			<div className="col-md-12">
				<form
					autocomplete="off"
					style={{ marginBottom: 10 }}
					onSubmit={"return false"}
				>
					<div className="autocomplete" style={{ width: 800 }}>
						<input
							type="file"
							onChange={this.fileSelectedHandler}
						/>
					</div>

					<p
						className="btn btn-primary"
						onClick={this.searchByImage}
						style={{
							marginTop: 10,
							marginBottom: "10px",
							// marginLeft: "10px",
						}}
					>
						Tìm kiếm bằng hình ảnh
					</p>
				</form>
				{previewImageURL.length != 0 ? (
					<div>
						<img
							src={previewImageURL}
							alt=""
							width="30%"
							style={{ marginLeft: "-55%" }}
						/>
					</div>
				) : (
					<div></div>
				)}
				{relevant_images.length == 0 ? (
					<div></div>
				) : (
					<div class="row">
						<div style={{ fontSize: "30px", marginTop: "50px",marginBottom:"30px" }}>
							Hình ảnh liên quan :
						</div>
						{relevant_images.slice(0, 12).map((item) => {
							// let temp = item["resultID"].split("/").slice(1, 3);
							// let newLink = temp[0] + "/" + temp[1];
							return (
								<a
									href={"article/" + item["article_id"]}
									target="_blank"
								>
									<img
										className="zoom"
										src={
											`${serverIpAdress}/get_image?image_url=` +
											// newLink
											item["resultID"]
										}
										style={{
											width: "23%",
											borderBottom: "2px",
											marginTop: "15px",
											marginLeft: "2px",
										}}
									/>
								</a>
							);
						})}
					</div>
				)}
				{predicted_label.length == 0 ? (
					<div></div>
				) : (
					<div style={{ fontSize: "30px", marginTop: "50px" }}>
						Địa danh : {predicted_label}
					</div>
				)}
				<div
					style={{
						width: "580px",
						textAlign: "left",
						margin: "auto",
					}}
				>
					{isLoading ? (
						<Spinner animation="border" variant="primary" />
					) : previous_search_key.length !== 0 &&
					  articlesForEachPage.length === 0 ? (
						<div style={{ fontSize: "20px", marginTop: "30px" }}>
							Không tìm thấy kết quả cho
							<span
								style={{ fontWeight: "bold", fontSize: "25px" }}
							>
								{" " +
									(previous_search_key.length < 75
										? previous_search_key
										: previous_search_key.slice(0, 75) +
										  "...")}
							</span>
						</div>
					) : (
						articlesForEachPage.map((item) => (
							<div>
								<div className="row">
									<div className="col-md-5">
										<img
											src={
												`${serverIpAdress}/get_image?image_url=` +
												item["images"][0]
											}
											style={{
												width: "100%",
												borderBottom: "2px",
												marginTop: "15px",
											}}
										/>
									</div>
									<div
										className="col-md-7"
										style={{
											fontSize: "20px",
											fontWeight: "400",
											color: "#087CCE",
											marginTop: "15px",
										}}
									>
										<a
											href={"article/" + item["id"]}
											target="_blank"
										>
											{item["title"]}
										</a>
										<div style={{ color: "black" }}>
											{item["type"]}
										</div>
										<div
											style={{
												fontSize: "14px",
												color: "black",
											}}
										>
											<div>
												{item["content"]
													? item["content"][0].slice(
															0,
															300
													  ) + "....."
													: ""}
											</div>
										</div>
									</div>
								</div>
							</div>
						))
					)}

					<div style={{ marginTop: "10px", marginBottom: "30px" }}>
						{!isLoading && total_pages != 0 && (
							<ul className="pagination">
								<li
									className={`page-item first-item ${
										current_page === 1 ? "disabled" : ""
									}`}
									onClick={() =>
										this.setState({ current_page: 1 })
									}
								>
									<a>First</a>
								</li>
								<li
									className={`page-item previous-item ${
										current_page === 1 ? "disabled" : ""
									}`}
									onClick={() =>
										this.setState({
											current_page: current_page - 1,
										})
									}
								>
									<a>Previous</a>
								</li>
								{temp_array_mark_current_page.map((page) => (
									<li
										key={page}
										className={`page-item number-item ${
											current_page === page
												? "active"
												: ""
										}`}
										onClick={() =>
											this.setState({
												current_page: page,
											})
										}
									>
										<a>{page}</a>
									</li>
								))}
								<li
									className={`page-item next-item ${
										current_page === total_pages
											? "disabled"
											: ""
									}`}
									onClick={() =>
										this.setState({
											current_page: current_page + 1,
										})
									}
								>
									<a>Next</a>
								</li>
								<li
									className={`page-item last-item ${
										current_page === total_pages
											? "disabled"
											: ""
									}`}
									onClick={() =>
										this.setState({
											current_page: total_pages,
										})
									}
								>
									<a>Last</a>
								</li>
							</ul>
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default Search;
