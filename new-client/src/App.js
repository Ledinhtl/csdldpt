import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./pages/Home";
import Article from "./pages/Article";
import SearchByImage from "./pages/SearchByImage";
import SearchByText from "./pages/SearchByText";

function App() {
	return (
		<div className="App">
			<Router>
				<Switch>
					{/* <Route path="/" exact component={Home} /> */}
					{/* <Route path="/search_text" exact component={SearchByText} /> */}
					<Route path="/" exact component={SearchByText} />
					<Route
						path="/search_image"
						exact
						component={SearchByImage}
					/>
					<Route path="/article/:id" component={Article} />
				</Switch>
			</Router>
		</div>
	);
}

export default App;
