import React, { Component } from "react";
// import Header from "../components/Header";
import axios from "axios";
import serverIpAdress from "../config/config";
import { Carousel } from "react-responsive-carousel";

class Article extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "",
			content: "",
			author: "",
			type: "",
			modified_date: "",
			origin_link: "",
			images: [],
		};
	}

	async componentDidMount() {
		const id = this.props.match.params.id;
		const res = (
			await axios.post(`${serverIpAdress}/search_article_by_id`, {
				q: id,
			})
		).data;
		console.log("response : ");
		console.log(res["docs"][0]);
		this.setState({
			title: res["docs"][0].title[0],
			content: res["docs"][0].content,
			author: res["docs"][0].author[0],
			modified_date: res["docs"][0].modified_date
				? res["docs"][0].modified_date[0]
				: "2019-12-30",
			origin_link: res["docs"][0].origin_link[0],
			images: res["docs"][0].images,
			type: res["docs"][0].type[0],
		});
		console.log(this.state);
	}

	render() {
		const {
			title,
			content,
			author,
			type,
			modified_date,
			origin_link,
			images,
		} = this.state;

		return (
			<div className="container">
				<div style={{ borderBottom: "1px solid #eee" }}>
					<h1>Hệ thống tìm kiếm địa danh</h1>
					<h3>Bài viết</h3>
				</div>
				<div className="row">
					<div
						className="col-lg-7 col-lg-offset-3"
						style={{
							textAlign: "left",
							lineHeight: 2.0,
							fontSize: 15,
						}}
					>
						<div
							style={{ marginTop: "10px", marginBottom: "10px" }}
						>
							{modified_date}
						</div>
						<h2
							style={{
								fontSize: "32px",
								lineHeight: "150%",
								fontWeight: "700",
								marginBottom: "30px",
								marginTop: "15px",
							}}
						>
							{title}
						</h2>
						<h4>Loại bài viết : {type}</h4>
						<div>
							<div className="row">
								{images.length != 0 ? (
									images.map((item) => {
										return (
											<img
												src={
													`${serverIpAdress}/get_image?image_url=` +
													item
												}
												className="col-md-6"
												style={{marginTop:"4px"}}
											/>
										);
									})
								) : (
									// <img src ="https://imge.com/wp-content/uploads/2019/01/imge-logo-blue2.png"/>
									<div></div>
								)}
							</div>
						</div>

						<div
							style={{
								fontSize: "18px",
								lineHeight: "160%",
								fontWeight: "400",
								marginBottom: "15px",
								marginTop:"10px"
							}}
						>
							{content}
						</div>
						<h4 style={{ marginTop: "20px", float: "right" }}>
							{author}
						</h4>
						<div
							style={{ marginTop: "75px", marginBottom: "100px" }}
						>
							{" "}
							Link gốc:{" "}
							<a href={origin_link} target="_blank">
								{origin_link}
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Article;
