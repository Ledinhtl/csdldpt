from __future__ import print_function

import flask
from flask_cors import CORS
import os
import cv2
from image_search_pipeline.descriptors import DetectAndDescribe
from image_search_pipeline.information_retrieval import BagOfVisualWords
from image_search_pipeline.information_retrieval import Searcher
from image_search_pipeline.information_retrieval import chi2_distance
from scipy.spatial import distance
from redis import Redis
from imutils.feature import FeatureDetector_create, DescriptorExtractor_create
import pickle
import imutils
# import json
import numpy as np

from underthesea import word_tokenize
from solr_connection import SolrConnection

solr_connection_instance = SolrConnection("http://localhost:8983/solr/da_phuong_tien/")

app = flask.Flask(__name__)

CORS(app, resources={r"/*": {"origins": "*"}})

# construct the argument parser and parse the argument
args = {"dataset": "Data", "features_db": "output/features.hdf5", "bovw_db": "output/bovw.hdf5",
        "codebook": "output/vocab.cpickle", "idf": None}

# initialize the keypoint detector, local invariant descriptor, descriptor pipeline,
# distance metric, and inverted document frequency array
detector = FeatureDetector_create("SURF")
descriptor = DescriptorExtractor_create("RootSIFT")
dad = DetectAndDescribe(detector, descriptor)
distanceMetric = chi2_distance
idf = None

# if the path to the inverted document frequency is array was supplied, then load
# idf array and update the distance metric
if args["idf"] is not None:
    idf = pickle.loads(open(args["idf"], "rb").read())
    distanceMetric = distance.cosine

# load the codebook vocabulary and initialize the BOVW transformer
vocab = pickle.loads(open(args["codebook"], "rb").read())
bovw = BagOfVisualWords(vocab)

# connect to redis and perform the search
redisDB = Redis(host="localhost", port=6379, db=0)
searcher = Searcher(redisDB, args["bovw_db"], args["features_db"], idf=idf,
                    distanceMetric=distanceMetric)


# searcher.finish()


@app.route("/", methods=["GET", "POST"])
def hello():
    print("Accessed to server")
    return {"response": "hi man!!!"}


@app.route("/search_article_by_id", methods=["POST"])
def search_article_by_id():
    print("-" * 60)
    print("SEARCH BY ID")
    print(flask.request.json)

    text = flask.request.json['q']
    # tokens = word_tokenize(text)
    tokens = [text]
    print(tokens)

    list_field = ['id']

    query = ''

    for i in range(len(list_field)):
        if i == 0:
            query += '( '
        else:
            query += ' AND ( '
        for token in tokens:
            query = query + list_field[i] + ':' + '_'.join(token.split(' ')) + ' '
        query += ' ) '

    specific_option = {
        'fl': '*,score',
        'rows': 1,
        'sort': 'score desc'
    }

    print(query)

    result = solr_connection_instance.search(query, **specific_option)
    print(len(result.docs))
    print("-" * 60)
    return {"docs": result.docs}


@app.route("/search_text", methods=["POST"])
def search_text():
    print("-" * 60)
    print("SEARCH TEXT")
    print(flask.request.json)

    text = flask.request.json['q']
    if len(text) == 0:
        return {"docs": []}

    tokens = word_tokenize(text)
    print(tokens)

    list_field = ['title_indexed', 'content_indexed', 'type_indexed']

    query = ''

    for i in range(len(list_field)):
        if i == 0:
            query += '( '
        else:
            query += ' OR ( '
        for token in tokens:
            query = query + list_field[i] + ':' + '_'.join(token.split(' ')) + ' '
        query += ' ) '

    specific_option = {
        'fl': '*,score',
        'rows': 100,
        'sort': 'score desc'
    }

    print(query)

    result = solr_connection_instance.search(query, **specific_option)
    print(len(result.docs))
    print("-" * 60)
    return {"docs": result.docs}


@app.route("/post_image", methods=["POST"])
def post_image():
    print("-" * 60)
    print("SEARCH IMAGE")
    print(flask.request.files.getlist("image"))
    image = flask.request.files['image']
    print(image)
    print(image.filename)
    image.save(os.path.join("./upload_image", image.filename))
    print("image saved")
    # image_read = cv2.imread("./upload_image/" + image.filename)

    image_read = cv2.imread(os.path.join("./upload_image", image.filename))
    print(image_read.shape)

    return {"docs": []}


@app.route("/get_image", methods=["GET"])
def get_image():
    print("-" * 60)
    print("GET IMAGE")
    image_url = flask.request.args.get('image_url')
    print(image_url)

    list_unloaded_images = {
        "DaoCayCoDuBai/(1).gif": "DaoCayCoDuBai/(5).jpeg",
        "CotCoHaNoi/(1).gif": "CotCoHaNoi/(125).jpg",
        "ChuaMotCot/(1).gif": "ChuaMotCot/(53).jpg",
        "KimTuThap/(1).gif": "KimTuThap/(85).jpg",
        "LangBac/(1).gif": "LangBac/(149).jpg",
        "CotCoHaNoi/(71).gif": "CotCoHaNoi/(96).jpg",
    }
    if image_url in list_unloaded_images:
        image_url = list_unloaded_images[image_url]

    return flask.send_file("Data/" + image_url)


@app.route("/search_image", methods=["POST"])
def search_image():
    print("-" * 60)
    print("SEARCH IMAGE")
    image = flask.request.files['image']
    print(image)
    print(image.filename)
    image.save(os.path.join("./upload_image", image.filename))
    print("image saved")

    queryImage = cv2.imread(os.path.join("./upload_image", image.filename))

    # get histogram for each image
    image_color_hist = []
    color = ('b', 'g', 'r')  # bgr type
    for i, col in enumerate(color):
        histr = cv2.calcHist([queryImage], [i], None, [256], [0, 256])
        histr = histr / np.sum(histr, dtype=np.float32)
        histr = histr.reshape((256,))
        image_color_hist.append(histr)

    queryImage = imutils.resize(queryImage, width=320)
    queryImage = cv2.cvtColor(queryImage, cv2.COLOR_BGR2GRAY)

    # extract features from the query image and construct a bag-of-visual-word from  it
    (_, descs) = dad.describe(queryImage)
    hist = bovw.describe(descs).tocoo()

    sr = searcher.search(hist, image_color_hist, numResults=20)
    print("[INFO] search took: {:.2f}s".format(sr.search_time))

    relevant_images = []

    # normalize score_sitf and score_color
    max_score_sitf = 0.0
    max_score_color = 0.0

    # loop over the individual results
    for (i, (score_sift, score_color, resultID, resultsIdx)) in enumerate(sr.results):
        # load the result image and display it
        print(
            "[RESULT] {result_num}. {result} . {index} - {score_sift:.2f} - {score_color:.2f} ".format(result_num=i + 1,
                                                                                                       result=resultID,
                                                                                                       index=resultsIdx,
                                                                                                       score_sift=score_sift,
                                                                                                       score_color=score_color))
        article_id = ""
        list_article_id = solr_connection_instance.search('images:"' + resultID+'"')
        if len(list_article_id.docs) != 0:
            article_id = list_article_id.docs[0]['id']

        relevant_images.append(
            {"score_sift": float(score_sift), "score_color": float(score_color), "resultID": resultID,
             "total_score": 0.0, "article_id": article_id})

        if max_score_color < float(score_color):
            max_score_color = score_color
        if max_score_sitf < float(score_sift):
            max_score_sitf = score_sift

        # result = cv2.imread("{}/{}".format(args["dataset"], resultID))

    # predict label which image belonged
    predicted_label = ""
    temp_label = {}
    weight_of_sitf_score = 0.9
    for item in relevant_images:
        # get total score from score_sitf and score_color
        item['score_sift'] /= max_score_sitf
        item['score_color'] /= max_score_color
        item['total_score'] = weight_of_sitf_score * item['score_sift'] + (1 - weight_of_sitf_score) * item[
            'score_color']

        token = item['resultID'].split("/")[0]
        if token in temp_label:
            temp_label[token] += 1
        else:
            temp_label[token] = 1

    relevant_images.sort(key=lambda x: x['total_score'], reverse=False)

    predicted_label = max(temp_label, key=temp_label.get)
    print(temp_label[predicted_label])
    print(predicted_label)

    similar_meaning_words = {
        "Angcovat": "Angkor wat",
        "BigBen": "tháp đồng hồ BigBen",
        "ChuaDongYenTu": "chùa Đồng Yên Tử",
        "ChuaMotCot": "chùa Một Cột",
        "CotCoHaNoi": "cột cờ Hà Nội",
        "DaoCayCoDuBai": "đảo cây cọ DuBai",
        "Eiffel": "Eiffel",
        "HoHoanKiem": "hồ Hoàn Kiếm",
        "KimTuThap": "Kim Tự Tháp",
        "LangBac": "lăng Bác",
        "NuiPhuSi": "núi Phú Sĩ",
        "NuiTongThong": "núi Tổng Thống",
        "NuThanTuDo": "Nữ Thần Tự Do",
        "Pisa": "Pisa",
        "QuocTuGiam": "Văn Miếu Quốc Tử Giám",
        "VanLyTruongThanh": "Vạn Lý Trường Thành",
        "VongXoayMatTroi": "vòng xoay Mặt Trời"

    }

    if predicted_label not in similar_meaning_words.keys():
        pass
    else:
        predicted_label = similar_meaning_words[predicted_label]

    # search text by predicted_label
    text = predicted_label
    if len(text) == 0:
        return {"relevant_images": relevant_images, "relevant_docs": [], "predicted_label": predicted_label}

    tokens = word_tokenize(text)
    print(tokens)

    list_field = ['title_indexed', 'content_indexed', 'type_indexed']

    query = ''

    for i in range(len(list_field)):
        if i == 0:
            query += '( '
        else:
            query += ' OR ( '
        for token in tokens:
            query = query + list_field[i] + ':' + '_'.join(token.split(' ')) + ' '
        query += ' ) '

    specific_option = {
        'fl': '*,score',
        'rows': 100,
        'sort': 'score desc'
    }

    print(query)

    result = solr_connection_instance.search(query, **specific_option)
    print(len(result.docs))
    print("-" * 60)

    return {"relevant_images": relevant_images, "relevant_docs": result.docs, "predicted_label": predicted_label}


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=9876)
