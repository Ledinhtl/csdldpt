import pysolr


class SolrConnection:
    def __init__(self, path, timeout=3000):
        self.path = path
        self.connection = pysolr.Solr(self.path, timeout=timeout)


    def search(self, q, **kwargs):
        response = self.connection.search(q=q, **kwargs)
        return response

