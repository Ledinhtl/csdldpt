# import packages
from .baseindexer import BaseIndexer
import numpy as np
import h5py
import sys


class FeatureIndexer(BaseIndexer):

    def __init__(self, dbPath, estNumImages=500, maxBufferSize=50000, dbResizeFactor=2,
                 verbose=True):
        # call the parent constructor
        super(FeatureIndexer, self).__init__(dbPath, estNumImages=estNumImages,
                                             maxBufferSize=maxBufferSize, dbResizeFactor=dbResizeFactor,
                                             verbose=verbose)

        # open the HDF5 database for writing and initialize the datasets within
        # the group
        self.db = h5py.File(self.dbPath, mode="w")
        self.imageIDDB = None
        self.indexDB = None
        self.featuresDB = None

        self.colorHistorgramDB_b = None
        self.colorHistorgramDB_g = None
        self.colorHistorgramDB_r = None

        # initialize the image IDs buffer, index buffer, and the keypoints +
        # features buffer, histogram buffer
        self.imageIDBuffer = []
        self.indexBuffer = []
        self.featuresBuffer = None

        self.colorHistorgramBuffer_b = []
        self.colorHistorgramBuffer_g = []
        self.colorHistorgramBuffer_r = []

        # initialize the total number of features in the buffer along with the
        # indexs dictionary
        self.totalFeatures = 0
        # index: an integer representing the current row (i.e. the next empty row) in both the image_ids and index datasets.
        # features: An integer representing the next empty row in the features dataset
        self.idxs = {"index": 0, "features": 0, "histogram": 0}

    def add(self, imageID, kps,
            features, hists):  # add an image and its associated keypoints and features to the HDF5 database
        # compute the starting and ending index for the features lookup
        start = self.idxs["features"] + self.totalFeatures
        end = start + len(features)

        # update the image IDs buffer, features buffer, and index buffer,
        # followed by incrementing the feature count
        self.imageIDBuffer.append(imageID)
        self.featuresBuffer = BaseIndexer.featureStack(np.hstack([kps, features]),
                                                       self.featuresBuffer)
        print(self.featuresBuffer.shape)
        self.indexBuffer.append((start, end))
        self.totalFeatures += len(features)

        # append 3 hists to colorHistorgramBuffer
        self.colorHistorgramBuffer_b.append(hists[0])
        self.colorHistorgramBuffer_g.append(hists[1])
        self.colorHistorgramBuffer_r.append(hists[2])

        # self.colorHistorgramBuffer_b = BaseIndexer.featureStack(hists[0], self.colorHistorgramBuffer_b)
        # self.colorHistorgramBuffer_g = BaseIndexer.featureStack(hists[1], self.colorHistorgramBuffer_g)
        # self.colorHistorgramBuffer_r = BaseIndexer.featureStack(hists[2], self.colorHistorgramBuffer_r)

        # check to see if we have reached the maximum buffer size
        if self.totalFeatures >= self.maxBufferSize:
            # if the databases have not been created yet, create them
            if None in (
                    self.imageIDDB, self.indexDB, self.featuresDB, self.colorHistorgramDB_b, self.colorHistorgramDB_g,
                    self.colorHistorgramDB_r):
                self._debug("initial buffer full")
                self._createDatasets()

            # write the buffers to file
            self._writeBuffers()

    def _createDatasets(self):
        # compute the average number of features extracted from the initial buffer
        # and use this number to determine the approximate number of features for
        # the entire dataset
        avgFeatures = self.totalFeatures / float(len(self.imageIDBuffer))
        approxFeatures = int(avgFeatures * self.estNumImages)

        # grab the feature vector size
        fvectorSize = self.featuresBuffer.shape[1]

        # handle h5py datatype for Python 2.7
        if sys.version_info[0] < 3:
            dt = h5py.special_dtype(vlen=unicode)

        # otherwise use a datatype compatible with Python 3.5
        else:
            dt = h5py.special_dtype(vlen=str)

        # initialize the datasets
        self._debug("creating datasets...")

        self.imageIDDB = self.db.create_dataset("image_ids", (self.estNumImages,),
                                                maxshape=(None,), dtype=dt)
        self.indexDB = self.db.create_dataset("index", (self.estNumImages, 2),
                                              maxshape=(None, 2), dtype="int")
        self.featuresDB = self.db.create_dataset("features",
                                                 (approxFeatures, fvectorSize), maxshape=(None, fvectorSize),
                                                 dtype="float")

        self.colorHistorgramDB_b = self.db.create_dataset("histogram_b", (self.estNumImages, 256), maxshape=(None, 256),
                                                          dtype="float")
        self.colorHistorgramDB_g = self.db.create_dataset("histogram_g", (self.estNumImages, 256), maxshape=(None, 256),
                                                          dtype="float")
        self.colorHistorgramDB_r = self.db.create_dataset("histogram_r", (self.estNumImages, 256), maxshape=(None, 256),
                                                          dtype="float")

    def _writeBuffers(self):
        # write the buffers to disk
        self._writeBuffer(self.imageIDDB, "image_ids", self.imageIDBuffer,
                          "index")
        self._writeBuffer(self.indexDB, "index", self.indexBuffer, "index")
        self._writeBuffer(self.featuresDB, "features", self.featuresBuffer,
                          "features")

        print("self.colorHistorgramBuffer_b.shape")
        print(len(self.colorHistorgramBuffer_b))
        print("self.colorHistorgramDB_b.shape")
        print(len(self.colorHistorgramDB_b.shape))

        self._writeBuffer(self.colorHistorgramDB_b, "histogram_b", self.colorHistorgramBuffer_b, "index")
        self._writeBuffer(self.colorHistorgramDB_g, "histogram_g", self.colorHistorgramBuffer_g, "index")
        self._writeBuffer(self.colorHistorgramDB_r, "histogram_r", self.colorHistorgramBuffer_r, "index")

        # increment the indexes
        self.idxs["index"] += len(self.imageIDBuffer)
        self.idxs["features"] += self.totalFeatures

        # self.idxs["histogram"] += len(self.colorHistorgramBuffer)

        # reset the buffers and feature counts
        self.imageIDBuffer = []
        self.indexBuffer = []
        self.featuresBuffer = None
        self.totalFeatures = 0

        self.colorHistorgramBuffer_g = []
        self.colorHistorgramBuffer_r = []
        self.colorHistorgramBuffer_b = []

    def finish(self):
        # if the databases have not been initialized, then the original
        # buffers were never filled up ==> large maxBufferSize, small estNumImages
        if None in (self.imageIDDB, self.indexDB, self.featuresDB, self.colorHistorgramDB_b, self.colorHistorgramDB_g,
                    self.colorHistorgramDB_r):
            self._debug("minimum init buffer not reached", msgType="[WARN]")
            self._createDatasets()

        # write any unempty buffers to file
        self._debug("writing un-empty buffers...")
        self._writeBuffers()

        # compact datasets
        self._debug("compacting datasets...")
        self._resizeDataset(self.imageIDDB, "image_ids", finished=self.idxs["index"])
        self._resizeDataset(self.indexDB, "index", finished=self.idxs["index"])
        self._resizeDataset(self.featuresDB, "features", finished=self.idxs["features"])

        self._resizeDataset(self.colorHistorgramDB_b, "histogram_b", finished=self.idxs["index"])
        self._resizeDataset(self.colorHistorgramDB_g, "histogram_g", finished=self.idxs["index"])
        self._resizeDataset(self.colorHistorgramDB_r, "histogram_r", finished=self.idxs["index"])

        # close the database
        self.db.close()
