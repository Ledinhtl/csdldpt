import pysolr
import json
from underthesea import word_tokenize

connection = pysolr.Solr("http://localhost:8983/solr/da_phuong_tien/")

f = open("Data_data.json")

data = json.load(f)

# print(data)

docs = [
    {
        **doc,
        "type_indexed": word_tokenize(doc['type'], format='text'),
        'title_indexed': word_tokenize(doc['title'], format='text'),
        'content_indexed': word_tokenize(doc['content'], format='text'),
    } for doc in data
]

connection.add(docs, commit=True)
