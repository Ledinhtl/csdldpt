# CSDLDPT

-đầu tiên, cần cài OpenCV phiên bản contrib 3.4.2.16
(https://pypi.org/project/opencv-contrib-python/3.4.2.16/)


-Cài redis, sau đó cd vào server folder, gõ command sau để tạo chỉ mục ngược của dữ liệu ảnh trên redis:
python3 build_redis_index.py --bovw_db output/bovw.hdf5

-Cài Solr ver 7.7.2, khởi động Solr, tạo new core tên da_phuong_tien, r dẩy dữ liệu text vào 
Solr bằng cách cd vào server/Data_text, sau đó gõ command : 
python3 add_docs.py

-cd vào server folder, gõ command sau để khởi động server API: 
python3 server

-cd vào client folder, lần lượt gõ command sau để khởi động client:
npm install,
npm start